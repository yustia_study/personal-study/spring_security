package com.study.security.member;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class MemberDTO {
	
	private Integer id;

    private String name;

    private String account;

    private String password;

    private LocalDateTime lastAccessDt;

    private LocalDateTime regDt;

    public Member toEntity() {
        return new Member(id, name, account, password);
    }

}
